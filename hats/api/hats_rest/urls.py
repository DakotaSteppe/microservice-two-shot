from django.urls import path
from .views import HatView

urlpatterns = [
    path('hats/', HatView.as_view(), name='hats_list_create'),
    path('hats/<int:id>/', HatView.as_view(), name='hat_delete'),
]
