from django.http import JsonResponse, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from .models import Hat
from .serializers import serialize_hat
import json

@method_decorator(csrf_exempt, name='dispatch')
class HatView(View):
    def get(self, request):
        hats = Hat.objects.all()
        serialized_hats = [serialize_hat(hat) for hat in hats]
        return JsonResponse(serialized_hats, safe=False)

    def post(self, request):
        data = json.loads(request.body)
        hat = Hat.objects.create(**data)
        return JsonResponse(serialize_hat(hat))

    def delete(self, request, id):
        hat = get_object_or_404(Hat, id=id)
        hat.delete()
        return HttpResponse(status=204)
