from django.db import models


# {
#     "fabric": "Cotton",
#     "style_name": "Fancy Pants",
#     "color": "Red",
#     "picture_url": "https://google.com",
#     "location_in_wardrobe": "My pants"
# }

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField()
    location_in_wardrobe = models.CharField(max_length=200)

    def __str__(self):
        return self.style_name


# class LocationVO(models.Model):
#     import_href = models.CharField(max_length=200, unique=True)
#     closet_name = models.CharField(max_length=100, null=True)
#     section_number = models.PositiveSmallIntegerField()
#     shelf_number = models.PositiveSmallIntegerField()