def serialize_hat(hat):
    return {
        'id': hat.id,
        'fabric': hat.fabric,
        'style_name': hat.style_name,
        'color': hat.color,
        'picture_url': hat.picture_url,
        'location_in_wardrobe': hat.location_in_wardrobe
    }
