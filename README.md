# Wardrobify

Team:

* Ben Spak - Hats Microservice!
* Dakota Steppe - Shoes Microservice!

## Design

## Shoes microservice

There are two Shoes microservices:

shoes/api is a Django application with a Django project and a Django app already created. The Django app is not installed in the Django project. There are no URLs, views, or models yet created. You must create these.
shoes/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file, shoes/poll/poller.py, that you must implement to pull Bin data from the Wardrobe API.
Your Django application has a common directory that contains a json.py file that has a ModelEncoder in it, if you want to use it.

You can access the Django shoe API application from your browser or Insomnia on port 8080.

The Shoe resource should track its manufacturer, its model name, its color, a URL for a picture, and the bin in the wardrobe where it exists.

You must create RESTful APIs to get a list of shoes, create a new shoe, and delete a shoe.
You must create React component(s) to show a list of all shoes and their details.
You must create React component(s) to show a form to create a new shoe.
You must provide a way to delete a shoe.
You must route the existing navigation links to your components.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
