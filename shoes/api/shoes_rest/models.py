from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse
# Create your models here.

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    bin_location = models.CharField(max_length=50)  # Assuming bin location as a string field, but you can choose an appropriate field type

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name}"
